FROM node 
# FROM base image the foundational back software that will be in the image and containers created from this image
# any image created from the Dockerfile WILL include node.js
COPY . /workspace
# COPY source destination  . means everything in the current directory
# source path to the local folder, Destination the file directory name in the container

WORKDIR /workspace
# The commands you run in your dockerfile. What directory in the container do you want to run them from?

EXPOSE 3000
# port 3000 on a container created from the image is accessible
RUN npm install
# a command to run when you create a container based off of this images
ENTRYPOINT [ "node", "index.js"]
