const {PubSub} = require('@google-cloud/pubsub');
const pubsub = new PubSub({projectId:'town-issue-review-board-p2'}) // needs to project ID. Project name and Project ID MAY be different. Must Check
const express = require("express");
const cors = require("cors")
const bunyan = require('bunyan');
const {LoggingBunyan} = require('@google-cloud/logging-bunyan')
const cloudLogger = new LoggingBunyan();

const app = express();
app.use(express.json()) // req.body was giving undefined without this, so this line is needed when reading in information from body in postman
app.use(cors());

const config = {
    name:'submit-issue-logs',
    //streams are where you log the data
    streams:[
        cloudLogger.stream('info')
    ]
}

const logger = bunyan.createLogger(config);

app.post("/submitissue", async (req, res) => {
    const body = req.body;

    if (!body.dateIssue || !body.issueDescription || !body.location || !body.issueType){
        logger.info("Invalid Issue Posted")
        res.status(406);
        res.send("Error: Missing Required Field")
    }

    const date = new Date();
    const datePosting = "" + date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate();
    const timestamp = Date.now();
    const dateIssue = body.dateIssue;
    const issueDescription = body.issueDescription;
    const location = body.location;
    const issueType = body.issueType;

    const sender = {
        datePosting: datePosting,
        dateIssue: dateIssue,
        issueDescription: issueDescription,
        location: location,
        issueType: issueType,
        timestamp: timestamp
    };
    const response = await pubsub.topic('issues').publishJSON(sender);
    res.status(201);
    res.send(response) // returns a big number ex: 3021381813193763
    logger.info("New Issue Posted Successfully")
});

const PORT = process.env.PORT || 3000;

app.listen( PORT, () => {
    console.log(`Application started on PORT ${PORT}`);
});
