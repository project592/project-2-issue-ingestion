provider "google" {
    project = "town-issue-review-board-p2"
    region = "us-central1"
    zone = "us-central1-c"
}

resource "google_cloud_run_service" "town-issue-injestion-service" {
    name     = "town-issue-injestion"
    location = "us-central1"

    template {
        spec {
            containers {
                image = "gcr.io/town-issue-review-board-p2/issue-ingestion-v7@sha256:2b06a15eb8a4c022a8d20ebfcbd0c62437cdd37c054c6a4dee9a246749b4795a"
            }
        }
    }

  traffic {
    percent         = 100
    latest_revision = true
  }
}